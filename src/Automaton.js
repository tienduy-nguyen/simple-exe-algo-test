export default class Automaton {
  readCommands(commands) {
    let state = 'q1';
    const dict = {
      'q1-1': 'q2',
      'q1-0': 'q1',
      'q2-0': 'q3',
      'q2-1': 'q2',
      'q3-0': 'q2',
      'q3-1': 'q2',
    };
    for (let c of commands) {
      const check = state + '-' + c.toString();
      state = dict[check];
    }
    return state === 'q2';
  }
}
